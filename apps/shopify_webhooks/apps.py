from django.apps import AppConfig


class ShopifyWebhooksConfig(AppConfig):
    name = 'shopify_webhooks'
