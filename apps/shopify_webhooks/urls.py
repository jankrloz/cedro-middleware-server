from django.conf.urls import include, url

urlpatterns = [
    url(r'^products/', include('apps.shopify_webhooks.products.urls', namespace='products')),
]
