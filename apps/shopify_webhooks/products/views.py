from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from shopify_webhook.decorators import webhook


@csrf_exempt
@webhook
def update(request):
    print(request.webhook_data)
    return HttpResponse('Product Updated!')
